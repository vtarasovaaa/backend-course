FROM python:3.10.0-slim

WORKDIR /code

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY Pipfile Pipfile.lock /code/
RUN pip install update pip && pip install pipenv && pipenv install

COPY . /code
