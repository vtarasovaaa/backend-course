# Backend course

### Перед запуском нужно проставить енвы

    export TELEGRAM_TOKEN=...
    export SECRET_KEY=...

## Запуск бота

    make start_bot

### Команды бота

- `/start` – сохраняет информацию о пользователе
- `/set_phone <phone>` – сохраняет номер телефона пользователя
- `/me` – выдает всю сохраненную информацию о пользователе
- `/account` – показывает все счета и карты


## API

### Эндпоинты

- `me/<telegram_id>`

response

    {
        "user":
        {
            "username": "str",
            "first_name": "str",
            "last_name": "str",
            "telegram_id": int,
            "phone": "str",
        }
    }


## Нагрузочное
https://overload.yandex.net/519988#tab=test_data&tags=&plot_groups=main&machines=&metrics=&slider_start=1651145555&slider_end=1651145615