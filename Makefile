migrate:
	docker-compose exec web pipenv run python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose exec web pipenv run python src/manage.py createsuperuser

createsuperuser_local:
	pipenv run python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml . --target-version py310

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml . --target-version py310

check_lint_ci:
	docker-compose run web pipenv run isort .
	docker-compose run web pipenv run flake8 --config setup.cfg
	docker-compose run web pipenv run black --config pyproject.toml . --target-version py310

start_bot:
	python src/manage.py start_bot

up:
	docker-compose up -d

build:
	docker-compose build

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}

down:
	docker-compose down

test:
	docker-compose run web pipenv run pytest src/tests

lint_ci:
	docker-compose run web make check_lint_ci
