import pytest

from app.internal.services.utils.validator import validate_phone_number


class TestValidatePhone:
    @pytest.mark.parametrize(
        ("phone_number", "is_valid"),
        [
            ("", False),
            ("kek", False),
            ("12345679", False),
            ("+7", False),
            ("+7123", False),
            ("8999000112278687654567898765", False),
            ("+79990001122", True),
            ("89990001122", True),
        ],
    )
    def test_serialize_user(self, phone_number, is_valid):
        assert validate_phone_number(phone_number) == is_valid
