import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.entities.account import CardInfo
from app.internal.services.storage.bank_account_storage import BankAccountStorage


@pytest.fixture(params=[0])
def account_from(request, test_user) -> BankAccount:
    if not isinstance(request.param, int):
        raise ValueError("amount mush be integer")
    return BankAccount.objects.create(number="111", amount=request.param, client=test_user)


@pytest.fixture(params=[0])
def account_to(request, second_test_user) -> BankAccount:
    if not isinstance(request.param, int):
        raise ValueError("amount mush be integer")
    return BankAccount.objects.create(number="222", amount=request.param, client=second_test_user)


@pytest.mark.django_db
class TestBankAccountStorage:
    @pytest.mark.parametrize(("account_from", "account_to"), [(10, 10)], indirect=True)
    @pytest.mark.parametrize(
        ("count", "expected_from_account_amount", "expected_to_account_amount"),
        [(5, 5, 15), (10, 0, 20), (0, 10, 10), (10000, 10, 10), (-1, 10, 10)],
    )
    def test_try_send_money(
        self,
        bank_account_storage: BankAccountStorage,
        account_from: BankAccount,
        account_to: BankAccount,
        count: int,
        expected_from_account_amount: int,
        expected_to_account_amount: int,
    ):
        bank_account_storage.try_send_money(account_from, account_to, count)

        account_from.refresh_from_db()
        account_to.refresh_from_db()

        assert account_from.amount == expected_from_account_amount
        assert account_to.amount == expected_to_account_amount

    def test_get_user_account_info_no_info(self, bank_account_storage: BankAccountStorage, test_user):
        account = bank_account_storage.get_user_account_info(test_user.telegram_id)

        assert not account

    def test_get_user_account_info_with_account(self, bank_account_storage: BankAccountStorage, test_user_with_account):
        account = bank_account_storage.get_user_account_info(test_user_with_account.telegram_id)

        assert account.number == test_user_with_account.account.number
        assert account.amount == test_user_with_account.account.amount
        assert account.cards == []

    def test_get_user_account_info_with_card(self, bank_account_storage: BankAccountStorage, test_user_with_card):
        account = bank_account_storage.get_user_account_info(test_user_with_card.telegram_id)

        assert account.number == test_user_with_card.account.number
        assert account.amount == test_user_with_card.account.amount
        assert account.cards == [CardInfo(number=card.number) for card in test_user_with_card.account.cards.all()]

    def test_user_has_card(self, bank_account_storage: BankAccountStorage, test_user_with_card):
        has_card = bank_account_storage.user_has_card(
            test_user_with_card.telegram_id, test_user_with_card.account.cards.first().number
        )

        assert has_card

    def test_user_has_card_no_card(self, bank_account_storage: BankAccountStorage, test_user):
        has_card = bank_account_storage.user_has_card(test_user.telegram_id, "123")

        assert not has_card

    def test_user_has_card_user_does_not_have_card(self, bank_account_storage: BankAccountStorage, test_user_with_card):
        has_card = bank_account_storage.user_has_card(123, test_user_with_card.account.cards.first().number)

        assert not has_card

    def test_user_has_account(self, bank_account_storage: BankAccountStorage, test_user_with_card):
        has_account = bank_account_storage.user_has_account(
            test_user_with_card.telegram_id, test_user_with_card.account.number
        )

        assert has_account

    def test_user_has_account_no_account(self, bank_account_storage: BankAccountStorage, test_user):
        has_account = bank_account_storage.user_has_account(test_user.telegram_id, "123")

        assert not has_account

    def test_user_has_account_user_does_not_have_accout(
        self, bank_account_storage: BankAccountStorage, test_user_with_card
    ):
        has_account = bank_account_storage.user_has_account(123, test_user_with_card.account.number)

        assert not has_account

    def test_get_card_transaction(
        self, bank_account_storage: BankAccountStorage, transactions, test_user_with_card, authorised_user_with_account
    ):
        transactions = bank_account_storage.get_card_transaction(test_user_with_card.account.cards.first().number)

        assert transactions[0].transfer_amount == 15
        assert transactions[0].receiver == test_user_with_card.username
        assert transactions[0].sender == authorised_user_with_account.username

    def test_get_account_transaction(
        self, bank_account_storage: BankAccountStorage, transactions, test_user_with_card, authorised_user_with_account
    ):
        transactions = bank_account_storage.get_account_transaction(test_user_with_card.account.number)

        assert len(transactions) == 2

        assert transactions[0].transfer_amount == 30
        assert transactions[0].receiver == test_user_with_card.username
        assert transactions[0].sender == authorised_user_with_account.username

        assert transactions[1].transfer_amount == 3
        assert transactions[1].receiver == authorised_user_with_account.username
        assert transactions[1].sender == test_user_with_card.username
