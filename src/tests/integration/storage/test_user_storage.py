import pytest

from app.internal.models.entities.telegram_user import TelegramUserModel


@pytest.mark.django_db
class TestUserStorage:
    @pytest.mark.parametrize("phone", ["", "89990001122"])
    def test_get_user_info(self, user_storage, test_user, phone):
        user_storage.set_phone_for_user(test_user.telegram_id, phone)

        test_user.refresh_from_db()

        assert user_storage.get_user_info(test_user.telegram_id) == TelegramUserModel(
            **{
                "username": test_user.username,
                "first_name": test_user.first_name,
                "last_name": test_user.last_name,
                "telegram_id": test_user.telegram_id,
                "phone": str(test_user.phone_number),
            }
        )

    @pytest.mark.parametrize("favorite_user_name", ["kek"])
    def test_add_unknown_favorite_user(self, user_storage, test_user, favorite_user_name):
        user_storage.add_favorite_user(test_user.telegram_id, favorite_user_name)

        assert list(test_user.favorites_users.all()) == []

    def test_add_favorite_user(self, user_storage, test_user, second_test_user):
        user_storage.add_favorite_user(test_user.telegram_id, second_test_user.username)

        assert list(test_user.favorites_users.all()) == [second_test_user]

    @pytest.mark.parametrize("favorite_user_name", ["kek"])
    def test_delete_unknown_favorite_user(
        self, user_storage, test_user_with_favorites, second_test_user, favorite_user_name
    ):
        user_storage.delete_favorite_user(test_user_with_favorites.telegram_id, favorite_user_name)

        assert list(test_user_with_favorites.favorites_users.all()) == [second_test_user]

    def test_delete_favorite_user(self, user_storage, test_user, second_test_user):
        user_storage.delete_favorite_user(test_user.telegram_id, second_test_user.username)

        assert list(test_user.favorites_users.all()) == []

    def test_get_favorite_user_unknown(self, faker, user_storage):
        users = user_storage.get_user_favorites(faker.random_int())
        assert not users

    def test_get_favorite_user_no_favorites(self, user_storage, test_user):
        users = user_storage.get_user_favorites(test_user.telegram_id)
        assert not users

    def test_get_favorite_user(self, user_storage, test_user_with_favorites, second_test_user):
        users = user_storage.get_user_favorites(test_user_with_favorites.telegram_id)
        assert users == [second_test_user.username]

    def test_get_account_transaction(
        self, user_storage, transactions, test_user_with_card, authorised_user_with_account
    ):
        transactions = user_storage.get_interacted_users(test_user_with_card.telegram_id)

        assert transactions == {authorised_user_with_account.username}
