import pytest

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.transaction import Transaction
from app.internal.models.user import User
from app.internal.services.bot_manager import BotManager
from app.internal.services.storage.bank_account_storage import BankAccountStorage
from app.internal.services.storage.user_storage import UserStorage


@pytest.fixture()
def test_user() -> User:
    return User.objects.create(
        username="test_user",
        first_name="test_first_user",
        last_name="test_last_user",
        telegram_id=12345678,
    )


@pytest.fixture()
def second_test_user() -> User:
    return User.objects.create(
        username="second_test_user",
        first_name="second_test_first_user",
        last_name="second_test_last_user",
        telegram_id=87654321,
    )


@pytest.fixture()
def test_user_with_account(faker, test_user) -> User:
    BankAccount.objects.create(number=faker.word(), amount=1000, client=test_user)
    return test_user


@pytest.fixture()
def test_user_with_card(faker, test_user_with_account) -> User:
    BankCard.objects.create(number=faker.word(), account=test_user_with_account.account)
    return test_user_with_account


@pytest.fixture()
def test_user_with_favorites(test_user, second_test_user) -> User:
    test_user.favorites_users.add(second_test_user)
    test_user.save()
    return test_user


@pytest.fixture()
def authorised_user(faker) -> User:
    return User.objects.create(
        username=faker.word(),
        first_name=faker.word(),
        last_name=faker.word(),
        telegram_id=faker.random_int(),
        phone_number="89990001122",
    )


@pytest.fixture()
def authorised_user_with_account_not_enough_money(faker, authorised_user) -> User:
    BankAccount.objects.create(number=faker.word(), amount=0, client=authorised_user)
    return authorised_user


@pytest.fixture()
def authorised_user_with_account(faker, authorised_user) -> User:
    BankAccount.objects.create(number=faker.word(), amount=faker.random_int(), client=authorised_user)
    return authorised_user


@pytest.fixture()
def authorised_user_with_friend(authorised_user, test_user) -> User:
    authorised_user.favorites_users.add(test_user)
    authorised_user.save()
    return authorised_user


@pytest.fixture()
def transactions(faker, authorised_user_with_account, test_user_with_card):
    Transaction.objects.create(
        account_from=authorised_user_with_account.account,
        card_to=test_user_with_card.account.cards.first(),
        transfer_amount=5,
    )
    Transaction.objects.create(
        account_from=authorised_user_with_account.account,
        card_to=test_user_with_card.account.cards.first(),
        transfer_amount=15,
    )

    Transaction.objects.create(
        account_from=test_user_with_card.account, account_to=authorised_user_with_account.account, transfer_amount=3
    )
    Transaction.objects.create(
        account_from=authorised_user_with_account.account, account_to=test_user_with_card.account, transfer_amount=30
    )


@pytest.fixture()
def user_storage() -> UserStorage:
    return UserStorage()


@pytest.fixture()
def bank_account_storage() -> BankAccountStorage:
    return BankAccountStorage()


@pytest.fixture()
def bot_manager(user_storage, bank_account_storage) -> BotManager:
    return BotManager(user_storage, bank_account_storage)
