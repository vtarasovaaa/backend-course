import pytest

from app.internal.models.entities.account import BankAccountInfo, CardInfo
from app.internal.services.bot_manager import Status


@pytest.mark.django_db
class TestBotManager:
    def test_start(self, bot_manager, user_model):
        response = bot_manager.start(user_model)

        assert response.status == Status.START

    @pytest.mark.parametrize(
        ("phone", "expected_status"), [("89990001122", Status.UPDATE_PHONE), ("kek", Status.WRONG_NUMBER)]
    )
    def test_set_phone(self, bot_manager, user_model, phone, expected_status):
        response = bot_manager.set_phone(user_model, phone)

        assert response.status == expected_status

    def test_me_authorised_user(self, bot_manager, authorised_user):
        response = bot_manager.me(authorised_user.telegram_id)

        assert response.status == Status.USER_INFO

    def test_me_unauthorised_user(self, bot_manager):
        response = bot_manager.me(123)

        assert response.status == Status.NO_PHONE

    def test_account_info_no_account(self, bot_manager, user_storage, authorised_user):
        response = bot_manager.account_info(authorised_user.telegram_id)

        assert response.status == Status.ACCOUNT_NOT_EXISTS
        assert not response.account

    def test_account_info_with_account(self, bot_manager, user_storage, authorised_user_with_account):
        response = bot_manager.account_info(authorised_user_with_account.telegram_id)

        assert response.status == Status.ACCOUNT_INFO
        assert response.account == BankAccountInfo(
            number=authorised_user_with_account.account.number,
            amount=authorised_user_with_account.account.amount,
            cards=[
                CardInfo(number=card["number"]) for card in authorised_user_with_account.account.cards.values("number")
            ],
        )

    def test_get_favorites(self, bot_manager, user_storage, test_user, authorised_user_with_friend):
        response = bot_manager.get_favorites(authorised_user_with_friend.telegram_id)

        assert response.status == Status.FAVORITES
        assert response.users == [test_user.username]

    def test_get_favorites_no_users(self, bot_manager, user_storage, test_user, authorised_user):
        response = bot_manager.get_favorites(authorised_user.telegram_id)

        assert response.status == Status.NOT_FAVORITES
        assert response.users == []

    def test_add_favorites(self, bot_manager, user_storage, authorised_user, test_user):
        response = bot_manager.add_favorites(authorised_user.telegram_id, test_user.username)

        assert response.status == Status.FAVORITE_ADDED

    def test_add_favorites_no_user(self, bot_manager, user_storage, authorised_user, faker):
        response = bot_manager.add_favorites(authorised_user.telegram_id, faker.word())

        assert response.status == Status.USER_NOT_EXISTS

    def test_delete_favorites(self, bot_manager, user_storage, authorised_user, test_user, faker):
        response = bot_manager.delete_favorites(authorised_user.telegram_id, test_user.username)
        assert response.status == Status.FAVORITE_DELETED

        response2 = bot_manager.delete_favorites(authorised_user.telegram_id, faker.word())
        assert response2.status == Status.FAVORITE_DELETED

    @pytest.mark.parametrize("receiver_name", ["receiver_name"])
    @pytest.mark.parametrize("count", [0])
    def test_send_to_no_receiver(self, bot_manager, authorised_user, receiver_name, count):
        response = bot_manager.send_to(authorised_user.telegram_id, receiver_name, count)

        assert response.status == Status.RECEIVER_ACCOUNT_NOT_EXISTS

    def test_send_to_no_receiver_account(self, bot_manager, authorised_user, test_user):
        response = bot_manager.send_to(authorised_user.telegram_id, test_user.username, 0)

        assert response.status == Status.RECEIVER_ACCOUNT_NOT_EXISTS

    def test_send_to_yourself(self, bot_manager, authorised_user_with_account):
        response = bot_manager.send_to(
            authorised_user_with_account.telegram_id, authorised_user_with_account.username, 0
        )

        assert response.status == Status.TRANSFER_TO_YOURSELF

    def test_send_to_no_money(self, bot_manager, authorised_user_with_account_not_enough_money, test_user_with_account):
        response = bot_manager.send_to(
            authorised_user_with_account_not_enough_money.telegram_id, test_user_with_account.username, 100
        )

        assert response.status == Status.NOT_ENOUGH_MONEY

    def test_send_to_success(self, bot_manager, authorised_user_with_account, test_user_with_account):
        response = bot_manager.send_to(authorised_user_with_account.telegram_id, test_user_with_account.username, 1)

        assert response.status == Status.SUCCESS_MONEY_TRANSFER

    def test_send_to_account_no_receiver_account(self, bot_manager, authorised_user_with_account):
        response = bot_manager.send_to_account(authorised_user_with_account.telegram_id, "123", 0)

        assert response.status == Status.RECEIVER_ACCOUNT_NOT_EXISTS

    def test_send_to_account_yourself(self, bot_manager, authorised_user_with_account):
        response = bot_manager.send_to_account(
            authorised_user_with_account.telegram_id, authorised_user_with_account.account.number, 0
        )

        assert response.status == Status.TRANSFER_TO_YOURSELF

    def test_send_to_account_no_money(
        self, bot_manager, authorised_user_with_account_not_enough_money, test_user_with_account
    ):
        response = bot_manager.send_to_account(
            authorised_user_with_account_not_enough_money.telegram_id, test_user_with_account.account.number, 100
        )

        assert response.status == Status.NOT_ENOUGH_MONEY

    def test_send_to_account_success(self, bot_manager, authorised_user_with_account, test_user_with_account):
        response = bot_manager.send_to_account(
            authorised_user_with_account.telegram_id, test_user_with_account.account.number, 1
        )

        assert response.status == Status.SUCCESS_MONEY_TRANSFER

    def test_send_to_card_no_receiver_account(self, bot_manager, authorised_user_with_account):
        response = bot_manager.send_to_card(authorised_user_with_account.telegram_id, "123", 0)

        assert response.status == Status.RECEIVER_CARD_NOT_EXISTS
