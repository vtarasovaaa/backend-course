import pytest

from app.internal.models.entities.telegram_user import TelegramUserModel


@pytest.fixture()
def user_model() -> TelegramUserModel:
    return TelegramUserModel(username="kek", first_name="first", last_name="last", telegram_id=1)
