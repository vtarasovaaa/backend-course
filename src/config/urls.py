from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from app.internal.transport.rest.api import api
from app.internal.transport.rest.webhooks import BotView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", api.urls),
    path("webhooks/", csrf_exempt(BotView.as_view())),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
