from typing import Optional

from django.db import transaction
from django.db.models import Q
from pydantic.types import SecretStr

from app.internal.models.entities.telegram_user import TelegramUserModel
from app.internal.models.transaction import Transaction
from app.internal.models.user import User


class UserStorage:
    def get_or_create_user(self, user_model: TelegramUserModel) -> User:
        user, _ = User.objects.get_or_create(
            username=user_model.username,
            first_name=user_model.first_name,
            last_name=user_model.last_name,
            telegram_id=user_model.telegram_id,
        )

        return user

    def get_user(self, telegram_id: int) -> Optional[User]:
        user = User.objects.filter(telegram_id=telegram_id)
        if not user.exists():
            return None
        return user.first()

    def get_user_by_username(self, username: str) -> Optional[User]:
        user = User.objects.filter(username=username)
        if not user.exists():
            return None
        return user.first()

    def get_user_by_phone(self, phone: str) -> Optional[User]:
        user = User.objects.filter(phone_number=phone)
        if not user.exists():
            return None
        return user.first()

    def check_password(self, user: User, password: str) -> Optional[User]:
        return user.check_password(password)

    @transaction.atomic()
    def set_phone_for_user(self, telegram_id: int, phone: str) -> bool:
        user = User.objects.select_for_update().filter(telegram_id=telegram_id)
        if not user.exists():
            return False
        user = user.first()
        user.phone_number = phone
        user.save(update_fields=["phone_number"])
        return True

    @transaction.atomic()
    def set_password_for_user(self, telegram_id: int, password: SecretStr) -> bool:
        user = User.objects.select_for_update().filter(telegram_id=telegram_id)
        if not user.exists():
            return False
        user = user.first()
        user.set_password(password.get_secret_value())
        user.save(update_fields=["password"])
        return True

    def get_user_info(self, telegram_id: int) -> TelegramUserModel:
        user = User.objects.filter(telegram_id=telegram_id).first()
        return TelegramUserModel(
            username=user.username,
            first_name=user.first_name,
            last_name=user.last_name,
            telegram_id=user.telegram_id,
            phone=user.phone_number,
        )

    def get_user_favorites(self, telegram_id: int) -> list[str]:
        users = User.objects.filter(telegram_id=telegram_id).values("favorites_users__username")
        if not users.exists():
            return []

        if not users[0]["favorites_users__username"]:
            return []

        return [user["favorites_users__username"] for user in users]

    @transaction.atomic()
    def add_favorite_user(self, telegram_id: int, favorite_user_name: str) -> bool:
        user = self.get_user(telegram_id)
        favorite_user = self.get_user_by_username(favorite_user_name)
        if not favorite_user:
            return False
        user.favorites_users.add(favorite_user)
        user.save()
        return True

    @transaction.atomic()
    def delete_favorite_user(self, telegram_id: int, favorite_user_name: str) -> None:
        user = self.get_user(telegram_id)
        favorite_user = self.get_user_by_username(favorite_user_name)
        user.favorites_users.remove(favorite_user)
        user.save()

    @transaction.atomic()
    def get_interacted_users(self, telegram_id: int) -> set[str]:
        transactions_from = Transaction.objects.filter(Q(account_from__client__telegram_id=telegram_id)).values(
            "card_to__account__client__username", "account_to__client__username"
        )

        transactions_to_card = Transaction.objects.filter(Q(card_to__account__client__telegram_id=telegram_id)).values(
            "account_from__client__username"
        )

        transactions_to_account = Transaction.objects.filter(Q(account_to__client__telegram_id=telegram_id)).values(
            "account_from__client__username"
        )

        all_users = set()

        for tr in transactions_from:
            if tr["card_to__account__client__username"]:
                all_users.add(tr["card_to__account__client__username"])
            if tr["account_to__client__username"]:
                all_users.add(tr["account_to__client__username"])

        for tr in transactions_to_card:
            if tr["account_from__client__username"]:
                all_users.add(tr["account_from__client__username"])

        for tr in transactions_to_account:
            if tr["account_from__client__username"]:
                all_users.add(tr["account_from__client__username"])

        return all_users
