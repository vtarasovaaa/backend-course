from django.db import transaction
from django.db.models import F, Q

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.entities.account import BankAccountInfo, CardInfo, TransactionInfo
from app.internal.models.transaction import Transaction


class BankAccountStorage:
    @transaction.atomic()
    def try_send_money(self, account_from: BankAccount, account_to: BankAccount, count: int) -> bool:
        if count < 0:
            return False

        if account_from.amount < count:
            return False

        account_from.amount = F("amount") - count
        account_to.amount = F("amount") + count
        account_from.save(update_fields=["amount"])
        account_to.save(update_fields=["amount"])
        return True

    def get_account(self, number: str) -> BankAccount | None:
        return BankAccount.objects.filter(number=number).first()

    @transaction.atomic()
    def get_account_by_card(self, number: str) -> BankAccount | None:
        account = BankCard.objects.filter(number=number)
        if not account.exists():
            return None
        return account.first().account

    def get_account_by_telegram_id(self, telegram_id: int) -> BankAccount:
        return BankAccount.objects.filter(client__telegram_id=telegram_id).first()

    def get_account_by_usernmae(self, username: str) -> BankAccount:
        return BankAccount.objects.filter(client__username=username).first()

    @transaction.atomic()
    def get_user_account_info(self, telegram_id: int) -> BankAccountInfo | None:
        accounts = BankAccount.objects.filter(client__telegram_id=telegram_id).values(
            "number", "amount", "cards__number"
        )
        if not accounts.exists():
            return None

        if not accounts[0]["cards__number"]:
            return BankAccountInfo(number=accounts[0]["number"], amount=accounts[0]["amount"], cards=[])

        return BankAccountInfo(
            number=accounts[0]["number"],
            amount=accounts[0]["amount"],
            cards=[CardInfo(number=account["cards__number"]) for account in accounts],
        )

    @transaction.atomic()
    def save_transaction(
        self, account_from: BankAccount, account_to: BankAccount, count: int, receiver_card: str | None
    ) -> None:
        if receiver_card:
            card_to = BankCard.objects.filter(number=receiver_card).first()
            self._save_card_transaction(account_from, card_to, count)
        else:
            self._save_transaction(account_from, account_to, count)

    def _save_transaction(self, account_from: BankAccount, account_to: BankAccount, count: int) -> None:
        Transaction.objects.create(account_from=account_from, account_to=account_to, transfer_amount=count)

    def _save_card_transaction(self, account_from: BankAccount, card_to: BankCard, count: int) -> None:
        Transaction.objects.create(account_from=account_from, card_to=card_to, transfer_amount=count)

    def user_has_card(self, telegram_id: int, card_number: str) -> bool:
        card = BankCard.objects.filter(Q(number=card_number) & Q(account__client__telegram_id=telegram_id))
        return card.exists()

    def user_has_account(self, telegram_id: int, account_number: str) -> bool:
        card = BankAccount.objects.filter(Q(number=account_number) & Q(client__telegram_id=telegram_id))
        return card.exists()

    def get_card_transaction(self, card_number: str):
        transactions = (
            Transaction.objects.filter(card_to__number=card_number)
            .order_by("-created_at")
            .values("transfer_amount", "account_from__client__username", "card_to__account__client__username")
        )

        return [
            TransactionInfo(
                receiver=tran["card_to__account__client__username"],
                sender=tran["account_from__client__username"],
                transfer_amount=tran["transfer_amount"],
            )
            for tran in transactions
        ]

    def get_account_transaction(self, account_number: str):
        transactions = (
            Transaction.objects.filter(Q(account_to__number=account_number) | Q(account_from__number=account_number))
            .order_by("-created_at")
            .values("transfer_amount", "account_from__client__username", "account_to__client__username")
        )

        return [
            TransactionInfo(
                receiver=tran["account_to__client__username"],
                sender=tran["account_from__client__username"],
                transfer_amount=tran["transfer_amount"],
            )
            for tran in transactions
        ]
