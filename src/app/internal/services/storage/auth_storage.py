from datetime import timedelta

import jwt
from django.db import transaction

from app.internal.models.entities.auth import AuthModel
from app.internal.models.issued_token import IssuedToken
from app.internal.models.user import User
from app.internal.services.utils.time import get_now, utc_from_timestamp
from config import settings


class AuthStorage:
    @transaction.atomic()
    def generate_access_and_refresh_tokens(self, user: User) -> AuthModel:
        access_token = self._generate_access_token(user.telegram_id)
        refresh_token = self._generate_refresh_token()

        IssuedToken.objects.create(jti=refresh_token, user=user)

        return AuthModel(access_token=access_token, refresh_token=refresh_token)

    def get_token(self, refresh_token: str) -> IssuedToken | None:
        token = IssuedToken.objects.filter(jti=refresh_token)
        if not token.exists():
            return None
        return token.first()

    def revoke_all_tokens(self, user: User) -> None:
        user.refresh_tokens.update(revoked=True)
        user.save()

    def revoke_token(self, issued_token: IssuedToken) -> None:
        issued_token.revoked = True
        issued_token.save(update_fields=["revoked"])

    def has_token_expired(self, token: str) -> bool:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return True

        exp_time = payload.get("exp")

        if exp_time is None:
            return True

        return utc_from_timestamp(exp_time) < get_now()

    def get_telegram_id(self, token: str) -> int | None:
        try:
            payload = jwt.decode(token, settings.JWT_SECRET_KEY, algorithms=["HS256"])
        except jwt.exceptions.ExpiredSignatureError:
            return None

        return payload.get("id")

    def _generate_access_token(self, telegram_id: int) -> str:
        dt = get_now() + timedelta(settings.JWT_ACCESS_TOKEN_EXP_TIME)

        return jwt.encode({"id": telegram_id, "exp": int(dt.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256")

    def _generate_refresh_token(self) -> str:
        dt = get_now() + timedelta(settings.JWT_REFRESH_TOKEN_EXP_TIME)

        return jwt.encode({"exp": int(dt.timestamp())}, settings.JWT_SECRET_KEY, algorithm="HS256")
