import enum
import functools
from typing import Optional

from pydantic.main import BaseModel
from pydantic.types import SecretStr

from app.internal.models.bank_account import BankAccount
from app.internal.models.entities.account import BankAccountInfo, TransactionInfo
from app.internal.models.entities.telegram_user import TelegramUserModel
from app.internal.models.user import User
from app.internal.services.storage.bank_account_storage import BankAccountStorage
from app.internal.services.storage.user_storage import UserStorage
from app.internal.services.utils.validator import validate_phone_number


class Status(str, enum.Enum):
    START = "START"
    WRONG_NUMBER = "WRONG_NUMBER"
    UPDATE_PHONE = "UPDATE_PHONE"
    SET_PASSWORD = "SET_PASSWORD"
    WRONG_PASSWORD = "WRONG_PASSWORD"
    USER_INFO = "USER_INFO"
    ACCOUNT_INFO = "ACCOUNT_INFO"
    NOT_FAVORITES = "NOT_FAVORITES"
    FAVORITES = "FAVORITES"
    FAVORITE_ADDED = "FAVORITE_ADDED"
    FAVORITE_DELETED = "FAVORITE_DELETED"
    TRANSFER_TO_YOURSELF = "TRANSFER_TO_YOURSELF"
    USER_NOT_EXISTS = "USER_NOT_EXISTS"
    ACCOUNT_NOT_EXISTS = "ACCOUNT_NOT_EXISTS"
    RECEIVER_ACCOUNT_NOT_EXISTS = "RECEIVER_ACCOUNT_NOT_EXISTS"
    RECEIVER_CARD_NOT_EXISTS = "RECEIVER_CARD_NOT_EXISTS"
    NOT_ENOUGH_MONEY = "NOT_ENOUGH_MONEY"
    SUCCESS_MONEY_TRANSFER = "SUCCESS_MONEY_TRANSFER"
    NO_PHONE = "NO_PHONE"
    UNKNOWN_CARD = "UNKNOWN_CARD"
    TRANSACTIONS_INFO = "TRANSACTIONS_INFO"
    UNKNOWN_ACCOUNT = "UNKNOWN_ACCOUNT"
    INTERACTED_USERS = "INTERACTED_USERS"


class BotManagerResponse(BaseModel):
    status: Status


class UserInfoResponse(BotManagerResponse):
    user: TelegramUserModel


class AccountInfoResponse(BotManagerResponse):
    account: Optional[BankAccountInfo]


class UsersInfoResponse(BotManagerResponse):
    users: list[str]


class TransactionInfoResponse(BotManagerResponse):
    transactions: list[TransactionInfo]


def is_authorised(func):
    @functools.wraps(func)
    def wrapped(self, telegram_id, *args, **kwargs):
        phone = User.objects.filter(telegram_id=telegram_id).values("phone_number")
        if not phone.exists():
            return BotManagerResponse(status=Status.NO_PHONE)

        return func(self, telegram_id, *args, **kwargs)

    return wrapped


class BotManager:
    def __init__(self, user_storage: UserStorage, account_storage: BankAccountStorage):
        self._user_storage = user_storage
        self._account_storage = account_storage

    def start(self, user_model: TelegramUserModel) -> BotManagerResponse:
        self._user_storage.get_or_create_user(user_model)
        return BotManagerResponse(status=Status.START)

    def set_phone(self, user_model: TelegramUserModel, phone: str) -> BotManagerResponse:
        self._user_storage.get_or_create_user(user_model)
        if not validate_phone_number(phone):
            return BotManagerResponse(status=Status.WRONG_NUMBER)

        self._user_storage.set_phone_for_user(user_model.telegram_id, phone)
        return BotManagerResponse(status=Status.UPDATE_PHONE)

    @is_authorised
    def set_password(self, telegram_id: int, password: SecretStr) -> BotManagerResponse:
        self._user_storage.get_user(telegram_id)

        if self._user_storage.set_password_for_user(telegram_id, password):
            return BotManagerResponse(status=Status.SET_PASSWORD)

        return BotManagerResponse(status=Status.WRONG_PASSWORD)

    @is_authorised
    def me(self, telegram_id: int) -> UserInfoResponse:
        user_model = self._user_storage.get_user_info(telegram_id)
        return UserInfoResponse(status=Status.USER_INFO, user=user_model)

    @is_authorised
    def account_info(self, telegram_id: int) -> AccountInfoResponse:
        account = self._account_storage.get_user_account_info(telegram_id=telegram_id)
        if not account:
            return AccountInfoResponse(status=Status.ACCOUNT_NOT_EXISTS, account=None)

        return AccountInfoResponse(status=Status.ACCOUNT_INFO, account=account)

    @is_authorised
    def get_favorites(self, telegram_id: int) -> UsersInfoResponse:
        favorites = self._user_storage.get_user_favorites(telegram_id)
        if not favorites:
            return UsersInfoResponse(status=Status.NOT_FAVORITES, users=[])
        return UsersInfoResponse(status=Status.FAVORITES, users=favorites)

    @is_authorised
    def add_favorites(self, telegram_id: int, username: str) -> BotManagerResponse:
        if self._user_storage.add_favorite_user(telegram_id, username):
            return BotManagerResponse(status=Status.FAVORITE_ADDED)
        return BotManagerResponse(status=Status.USER_NOT_EXISTS)

    @is_authorised
    def delete_favorites(self, telegram_id: int, username: str) -> BotManagerResponse:
        self._user_storage.delete_favorite_user(telegram_id, username)
        return BotManagerResponse(status=Status.FAVORITE_DELETED)

    @is_authorised
    def send_to(self, telegram_id: int, receiver_name: str, count: int) -> BotManagerResponse:
        account_to = self._account_storage.get_account_by_usernmae(receiver_name)
        if account_to is None:
            return BotManagerResponse(status=Status.RECEIVER_ACCOUNT_NOT_EXISTS)

        return self._transfer_money(telegram_id, account_to, count)

    @is_authorised
    def send_to_account(self, telegram_id: int, receiver_account: str, count: int) -> BotManagerResponse:
        account_to = self._account_storage.get_account(receiver_account)
        if account_to is None:
            return BotManagerResponse(status=Status.RECEIVER_ACCOUNT_NOT_EXISTS)

        return self._transfer_money(telegram_id, account_to, count)

    @is_authorised
    def send_to_card(self, telegram_id: int, receiver_card: str, count: int) -> BotManagerResponse:
        account_to = self._account_storage.get_account_by_card(receiver_card)
        if account_to is None:
            return BotManagerResponse(status=Status.RECEIVER_CARD_NOT_EXISTS)

        return self._transfer_money(telegram_id, account_to, count, receiver_card)

    def _transfer_money(
        self, telegram_id: int, account_to: BankAccount, count: int, receiver_card: Optional[str] = None
    ) -> BotManagerResponse:
        if account_to.client.telegram_id == telegram_id:
            return BotManagerResponse(status=Status.TRANSFER_TO_YOURSELF)

        account_from = self._account_storage.get_account_by_telegram_id(telegram_id)
        if account_from is None:
            return BotManagerResponse(status=Status.ACCOUNT_NOT_EXISTS)

        if not self._account_storage.try_send_money(account_from, account_to, count):
            return BotManagerResponse(status=Status.NOT_ENOUGH_MONEY)

        self._account_storage.save_transaction(account_from, account_to, count, receiver_card)
        return BotManagerResponse(status=Status.SUCCESS_MONEY_TRANSFER)

    @is_authorised
    def get_card_statement(self, telegram_id: int, card_number: str) -> TransactionInfoResponse:
        has_card = self._account_storage.user_has_card(telegram_id, card_number)
        if not has_card:
            return TransactionInfoResponse(status=Status.UNKNOWN_CARD, transactions=[])

        transactions = self._account_storage.get_card_transaction(card_number)
        return TransactionInfoResponse(status=Status.TRANSACTIONS_INFO, transactions=transactions)

    @is_authorised
    def get_account_statement(self, telegram_id: int, account_number: str) -> TransactionInfoResponse:
        has_card = self._account_storage.user_has_account(telegram_id, account_number)
        if not has_card:
            return TransactionInfoResponse(status=Status.UNKNOWN_ACCOUNT, transactions=[])

        transactions = self._account_storage.get_account_transaction(account_number)
        return TransactionInfoResponse(status=Status.TRANSACTIONS_INFO, transactions=transactions)

    @is_authorised
    def interacted_users(self, telegram_id: int) -> UsersInfoResponse:
        return UsersInfoResponse(
            status=Status.INTERACTED_USERS, users=self._user_storage.get_interacted_users(telegram_id)
        )
