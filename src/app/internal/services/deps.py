import functools

from app.internal.services.bot_manager import BotManager
from app.internal.services.storage.auth_storage import AuthStorage
from app.internal.services.storage.bank_account_storage import BankAccountStorage
from app.internal.services.storage.user_storage import UserStorage


@functools.cache
def bot_manager() -> BotManager:
    return BotManager(user_storage(), bank_account_storage())


@functools.cache
def user_storage() -> UserStorage:
    return UserStorage()


@functools.cache
def auth_storage() -> AuthStorage:
    return AuthStorage()


@functools.cache
def bank_account_storage() -> BankAccountStorage:
    return BankAccountStorage()
