import datetime
from typing import Union

import pytz


def get_now() -> datetime.datetime:
    return pytz.UTC.localize(datetime.datetime.utcnow())


def utc_from_timestamp(timestamp: Union[float, int]) -> datetime.datetime:
    return datetime.datetime.fromtimestamp(timestamp, tz=pytz.UTC)
