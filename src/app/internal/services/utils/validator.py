import re

PHONE_RE = r"^\+?1?\d{9,15}$"
_PHONE_COMPILE_RE = re.compile(PHONE_RE)

BANK_ACCOUNT_NUMBER_RE = r"\d{20}"
BANK_CARD_NUMBER_RE = r"\d{16}"


def validate_phone_number(phone_number: str) -> bool:
    if _PHONE_COMPILE_RE.fullmatch(phone_number) is None:
        return False

    return True
