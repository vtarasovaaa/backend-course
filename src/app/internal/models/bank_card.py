from django.core.validators import RegexValidator
from django.db import models

from app.internal.models.bank_account import BankAccount
from app.internal.services.utils.validator import BANK_CARD_NUMBER_RE


class BankCard(models.Model):
    number_regex = RegexValidator(regex=BANK_CARD_NUMBER_RE, message="card must contains 16 digits")
    number = models.CharField(unique=True, db_index=True, validators=[number_regex], max_length=16)

    account = models.ForeignKey(to=BankAccount, on_delete=models.CASCADE, related_name="cards")

    objects = models.Manager()

    def __str__(self):
        return f"BankCard: {self.number}"

    class Meta:
        db_table = "bank_card"
