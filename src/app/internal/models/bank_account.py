from django.core.validators import MinValueValidator, RegexValidator
from django.db import models

from app.internal.models.user import User
from app.internal.services.utils.validator import BANK_ACCOUNT_NUMBER_RE


class BankAccount(models.Model):
    number_regex = RegexValidator(regex=BANK_ACCOUNT_NUMBER_RE, message="account contains 20 digits")
    number = models.CharField(unique=True, db_index=True, validators=[number_regex], max_length=20)

    amount = models.DecimalField(max_digits=11, decimal_places=2, validators=[MinValueValidator(limit_value=0)])

    client = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name="account")

    objects = models.Manager()

    def __str__(self):
        return f"BankAccount: {self.number}"

    class Meta:
        db_table = "bank_account"
