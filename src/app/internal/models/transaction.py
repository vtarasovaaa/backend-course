from django.core.validators import MinValueValidator
from django.db import models

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard


class Transaction(models.Model):
    account_from = models.ForeignKey(
        to=BankAccount, null=True, on_delete=models.SET_NULL, related_name="transactions_from"
    )
    card_to = models.ForeignKey(to=BankCard, null=True, on_delete=models.SET_NULL, related_name="transactions_to")
    account_to = models.ForeignKey(to=BankAccount, null=True, on_delete=models.SET_NULL, related_name="transactions_to")

    transfer_amount = models.DecimalField(
        max_digits=11, decimal_places=2, validators=[MinValueValidator(limit_value=0)]
    )

    created_at = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()

    class Meta:
        db_table = "transaction"
        constraints = [
            models.CheckConstraint(
                name="card_to_or_account_to",
                check=(
                    models.Q(card_to__isnull=True, account_to__isnull=False)
                    | models.Q(card_to__isnull=False, account_to__isnull=True)
                ),
            )
        ]
