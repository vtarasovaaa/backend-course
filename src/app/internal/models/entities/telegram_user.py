from typing import Optional, cast

from pydantic.main import BaseModel


class TelegramUserModel(BaseModel):
    telegram_id: int
    username: str
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    phone: Optional[str] = None

    @property  # todo нужен тест
    def full_name(self) -> str:
        if self.first_name is None and self.last_name is None:
            return "Anonymous"
        if self.last_name is None:
            return cast(str, self.first_name)
        if self.first_name is None:
            return cast(str, self.last_name)

        return f"{self.first_name} {self.last_name}"
