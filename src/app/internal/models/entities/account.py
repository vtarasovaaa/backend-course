from pydantic.main import BaseModel


class CardInfo(BaseModel):
    number: str


class BankAccountInfo(BaseModel):
    number: str
    amount: float
    cards: list[CardInfo]


class TransactionInfo(BaseModel):
    receiver: str
    sender: str
    transfer_amount: float
