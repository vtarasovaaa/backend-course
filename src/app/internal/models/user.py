from django.contrib.auth.models import AbstractBaseUser
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from app.internal.services.utils.validator import PHONE_RE


class User(AbstractBaseUser):
    telegram_id = models.IntegerField(unique=True, db_index=True)
    username = models.CharField(max_length=250)
    first_name = models.CharField(max_length=250, null=True, blank=True)
    last_name = models.CharField(max_length=250, null=True, blank=True)
    phone_regex = RegexValidator(regex=PHONE_RE, message="Phone number must be entered in the format: '+999999999'.")
    phone_number = models.CharField(unique=True, validators=[phone_regex], max_length=17, null=True)

    password = models.CharField(_("password"), max_length=128, null=True)

    favorites_users = models.ManyToManyField(to="self", symmetrical=False)

    objects = models.Manager()

    def __str__(self):
        return f"User: {self.username}"

    class Meta:
        db_table = "tg_user"
        unique_together = ("telegram_id", "username", "first_name", "last_name")
