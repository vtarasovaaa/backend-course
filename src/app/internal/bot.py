import json

import telegram
from telegram.ext import CommandHandler, Dispatcher, Updater

from app.internal.transport.bot.handlers import (
    account_info,
    account_statement,
    add_favorites,
    card_statement,
    delete_favorites,
    interacted_users,
    me,
    send_to,
    send_to_account,
    send_to_card,
    set_password,
    set_phone,
    show_favorites,
    start,
)
from config.settings import TELEGRAM_TOKEN

_BOT_COMMANDS = {
    "start": start,
    "set_phone": set_phone,
    "set_password": set_password,
    "me": me,
    "account": account_info,
    "show": show_favorites,
    "add": add_favorites,
    "delete": delete_favorites,
    "send_to": send_to,
    "send_to_account": send_to_account,
    "send_to_card": send_to_card,
    "card_statement": card_statement,
    "account_statement": account_statement,
    "interacted_users": interacted_users,
}


# polling
def start_bot():
    updater = Updater(TELEGRAM_TOKEN, use_context=True)

    for command_name, command in _BOT_COMMANDS.items():
        updater.dispatcher.add_handler(CommandHandler(command_name, command))

    updater.start_polling()
    updater.idle()


# webhook
class TelegramBot:
    def __init__(self, token: str):
        self._bot = telegram.Bot(token=token)
        self._dispatcher = Dispatcher(self._bot, None, workers=0)

        for command_name, command in _BOT_COMMANDS.items():
            self._dispatcher.add_handler(CommandHandler(command_name, command))

    def update(self, text: str):
        update = telegram.Update.de_json(json.loads(text), self._bot)
        self._dispatcher.process_update(update)


BOT = TelegramBot(token=TELEGRAM_TOKEN)
