from django.contrib import admin

from app.internal.models.bank_account import BankAccount
from app.internal.models.bank_card import BankCard
from app.internal.models.transaction import Transaction
from app.internal.models.user import User


@admin.register(User)
class AdminUser(admin.ModelAdmin):
    list_display = ("telegram_id", "username", "first_name", "last_name", "phone_number")


@admin.register(BankCard)
class AdminBankCard(admin.ModelAdmin):
    list_display = ("number", "get_account_number", "get_account_amount")

    def get_account_number(self, obj):
        return obj.account.number

    def get_account_amount(self, obj):
        return obj.account.amount

    get_account_number.short_description = "Account number"
    get_account_amount.short_description = "Account amount"


@admin.register(BankAccount)
class AdminBankAccount(admin.ModelAdmin):
    list_display = ("get_client_telegram_id", "number", "amount")

    def get_client_telegram_id(self, obj):
        return obj.client.telegram_id

    get_client_telegram_id.short_description = "Client id"


@admin.register(Transaction)
class AdminTransaction(admin.ModelAdmin):
    list_display = ("get_account_from_number", "get_account_to_number", "get_card_to_number", "transfer_amount")

    def get_account_from_number(self, obj):
        return obj.account_from.number

    def get_account_to_number(self, obj):
        if not obj.account_to:
            return ""
        return obj.account_to.number

    def get_card_to_number(self, obj):
        if not obj.card_to:
            return ""
        return obj.card_to.number

    get_account_from_number.short_description = "Account from number"
    get_account_to_number.short_description = "Account to number"
    get_card_to_number.short_description = "Card to number"
