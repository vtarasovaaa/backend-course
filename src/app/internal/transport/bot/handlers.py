from pydantic.types import SecretStr
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.models.entities.telegram_user import TelegramUserModel
from app.internal.services.bot_manager import Status, TransactionInfoResponse
from app.internal.services.deps import bot_manager


def start(update: Update, context: CallbackContext) -> None:
    response = bot_manager().start(
        TelegramUserModel(
            username=update.effective_user.username,
            first_name=update.effective_user.first_name,
            last_name=update.effective_user.last_name,
            telegram_id=update.effective_user.id,
        )
    )
    username = update.effective_user.username
    match response.status:
        case Status.START:
            update.message.reply_text(f"Привет, {username}!")
        case _:
            update.message.reply_text("Попробуй позже")


def set_phone(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Что-то не похоже на номер, вызови команду в формате /set_phone +799999999")
        return

    response = bot_manager().set_phone(
        TelegramUserModel(
            username=update.effective_user.username,
            first_name=update.effective_user.first_name,
            last_name=update.effective_user.last_name,
            telegram_id=update.effective_user.id,
        ),
        context.args[0],
    )
    match response.status:
        case Status.UPDATE_PHONE:
            update.message.reply_text("Обновил твой номер телефона")
        case Status.WRONG_NUMBER:
            update.message.reply_text("Что-то не похоже на номер, напиши, пожалуйста, номер начиная с +7...")
        case _:
            update.message.reply_text("Попробуй позже")


def set_password(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Вызови команду в формате /set_password password")
        return

    response = bot_manager().set_password(update.effective_user.id, SecretStr(context.args[0]))
    match response.status:
        case Status.SET_PASSWORD:
            update.message.reply_text("Установил пароль")
        case Status.WRONG_PASSWORD:
            update.message.reply_text("Что-то пошло не так")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def me(update: Update, context: CallbackContext) -> None:
    response = bot_manager().me(update.effective_user.id)
    match response.status:
        case Status.USER_INFO:
            update.message.reply_text(
                f"Вот что я знаю о тебе:\n"
                f"id: {response.user.telegram_id}\n"
                f"username: {response.user.username}\n"
                f"имя: {response.user.full_name}\n"
                f"номер телефона: {response.user.phone}\n"
            )
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def account_info(update: Update, context: CallbackContext) -> None:
    response = bot_manager().account_info(update.effective_user.id)
    match response.status:
        case Status.ACCOUNT_INFO:
            data = [f"Номер счета: {response.account.number}", f"Сумма на счету: {response.account.amount}"]
            for card in response.account.cards:
                data.append(f"Номер карты: {card.number}")

            update.message.reply_text("\n".join(data))
        case Status.ACCOUNT_NOT_EXISTS:
            update.message.reply_text("У тебя нет счета")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def show_favorites(update: Update, context: CallbackContext) -> None:
    response = bot_manager().get_favorites(update.effective_user.id)
    match response.status:
        case Status.FAVORITES:
            users = "\n".join(response.users)
            update.message.reply_text(f"Избранные пользователи:\n{users}")
        case Status.NOT_FAVORITES:
            update.message.reply_text("Ты никого не добавлял в избранное, воспользуйся командой /add")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def add_favorites(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Что-то не похоже на имя, вызови команду в формате /add bot")
        return

    username = context.args[0].lstrip("@")

    response = bot_manager().add_favorites(update.effective_user.id, username)
    match response.status:
        case Status.FAVORITE_ADDED:
            update.message.reply_text("Добавил")
        case Status.USER_NOT_EXISTS:
            update.message.reply_text(f"Нет пользователя с именем {username}")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def delete_favorites(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Что-то не похоже на имя, вызови команду в формате /delete bot")
        return

    response = bot_manager().delete_favorites(update.effective_user.id, context.args[0].lstrip("@"))
    match response.status:
        case Status.FAVORITE_DELETED:
            update.message.reply_text("Удалил")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def send_to(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 2 or not context.args[1].isdigit():
        update.message.reply_text("Передай кому и сколько хочешь перевести, например, /send_to bot 30")
        return

    receiver_name = context.args[0].lstrip("@")
    count = int(context.args[1])

    response = bot_manager().send_to(update.effective_user.id, receiver_name, count)
    match response.status:
        case Status.RECEIVER_ACCOUNT_NOT_EXISTS:
            update.message.reply_text(f"У {receiver_name} нет счета")
        case Status.ACCOUNT_NOT_EXISTS:
            update.message.reply_text("У тебя нет счета")
        case Status.TRANSFER_TO_YOURSELF:
            update.message.reply_text("Нельзя перевести себе")
        case Status.NOT_ENOUGH_MONEY:
            update.message.reply_text("Недостаточно средств")
        case Status.SUCCESS_MONEY_TRANSFER:
            update.message.reply_text("Перевел")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def send_to_account(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 2 or not context.args[1].isdigit():
        update.message.reply_text(
            "Передай на какой счет и сколько хочешь перевести, например, /send_to_account 00000000 30"
        )
        return

    receiver_account = context.args[0]
    count = int(context.args[1])

    response = bot_manager().send_to_account(update.effective_user.id, receiver_account, count)
    match response.status:
        case Status.RECEIVER_ACCOUNT_NOT_EXISTS:
            update.message.reply_text(f"Нет счета {receiver_account}")
        case Status.ACCOUNT_NOT_EXISTS:
            update.message.reply_text("У тебя нет счета")
        case Status.TRANSFER_TO_YOURSELF:
            update.message.reply_text("Нельзя перевести себе")
        case Status.NOT_ENOUGH_MONEY:
            update.message.reply_text("Недостаточно средств")
        case Status.SUCCESS_MONEY_TRANSFER:
            update.message.reply_text("Перевел")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def send_to_card(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 2 or not context.args[1].isdigit():
        update.message.reply_text(
            "Передай на какую карту и сколько хочешь перевести, например, /send_to_card 00000000 30"
        )
        return

    receiver_card = context.args[0]
    count = int(context.args[1])

    response = bot_manager().send_to_card(update.effective_user.id, receiver_card, count)
    match response.status:
        case Status.RECEIVER_CARD_NOT_EXISTS:
            update.message.reply_text(f"Нет карты {receiver_card}")
        case Status.ACCOUNT_NOT_EXISTS:
            update.message.reply_text("У тебя нет счета")
        case Status.TRANSFER_TO_YOURSELF:
            update.message.reply_text("Нельзя перевести себе")
        case Status.NOT_ENOUGH_MONEY:
            update.message.reply_text("Недостаточно средств")
        case Status.SUCCESS_MONEY_TRANSFER:
            update.message.reply_text("Перевел")
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def card_statement(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Про какую карту ты хочешь узнать? Напиши команду так: /card_statement 00000000")
        return

    card = context.args[0]

    response: TransactionInfoResponse = bot_manager().get_card_statement(update.effective_user.id, card)
    match response.status:
        case Status.UNKNOWN_CARD:
            update.message.reply_text(f"У тебя нет карты {card}")
        case Status.TRANSACTIONS_INFO:
            if not response.transactions:
                update.message.reply_text("Нет информации о переводах")
                return
            data = []
            for tr in response.transactions:
                data.append(f"от: {tr.sender}")
                data.append(f"кому: {tr.receiver}")
                data.append(f"сумма: {tr.transfer_amount}")
                data.append("---------------------------")
            update.message.reply_text("\n".join(data))
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def account_statement(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text("Про какой счет ты хочешь узнать? Напиши команду так: /account_statement 00000000")
        return

    account = context.args[0]

    response: TransactionInfoResponse = bot_manager().get_account_statement(update.effective_user.id, account)
    match response.status:
        case Status.UNKNOWN_ACCOUNT:
            update.message.reply_text(f"У тебя нет счета {account}")
        case Status.TRANSACTIONS_INFO:
            if not response.transactions:
                update.message.reply_text("Нет информации о переводах")
                return
            data = []
            for tr in response.transactions:
                data.append(f"от: {tr.sender}")
                data.append(f"кому: {tr.receiver}")
                data.append(f"сумма: {tr.transfer_amount}")
                data.append("---------------------------")
            update.message.reply_text("\n".join(data))
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")


def interacted_users(update: Update, context: CallbackContext) -> None:
    response = bot_manager().interacted_users(update.effective_user.id)
    match response.status:
        case Status.INTERACTED_USERS:
            if not response.users:
                update.message.reply_text("Нет информации о переводах")
                return
            update.message.reply_text("\n".join(response.users))
        case Status.NO_PHONE:
            update.message.reply_text(
                "Сначала нужно заполнить информацию о себе, введи свой номер телефона с помощью /set_phone"
            )
        case _:
            update.message.reply_text("Попробуй позже")
