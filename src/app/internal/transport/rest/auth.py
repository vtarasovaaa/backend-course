from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.services.deps import auth_storage, user_storage


class HTTJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        if auth_storage().has_token_expired(token):
            return None

        telegram_id = auth_storage().get_telegram_id(token)
        if not telegram_id:
            return None

        user_model = user_storage().get_user_info(telegram_id)
        request.user = user_model

        return token
