from typing import Union

from django.http import HttpRequest, HttpResponseNotFound, JsonResponse
from django.views import View

from app.internal.bot import BOT


class BotView(View):
    def post(self, request: HttpRequest) -> Union[JsonResponse, HttpResponseNotFound]:
        BOT.update(request.body)
        return JsonResponse({"OK": "POST request processed"})
