from http import HTTPStatus

from django.http import HttpRequest, HttpResponseBadRequest, HttpResponseNotFound, JsonResponse
from ninja import NinjaAPI
from pydantic.main import BaseModel

from app.internal.models.entities.auth import AuthModel
from app.internal.models.entities.telegram_user import TelegramUserModel
from app.internal.services.deps import auth_storage, user_storage
from app.internal.transport.rest.auth import HTTJWTAuth

api = NinjaAPI()


class UserModel(BaseModel):
    phone: str
    password: str


class RefreshModel(BaseModel):
    refresh_token: str


@api.get("/me", auth=HTTJWTAuth())
def me(request: HttpRequest) -> TelegramUserModel | HttpResponseNotFound:
    db_user = user_storage().get_user(request.user.telegram_id)
    if db_user is None or not db_user.phone_number:
        return HttpResponseNotFound("no user or user's phone")

    return user_storage().get_user_info(request.user.telegram_id)


@api.post("/login")
def login(request: HttpRequest, user: UserModel) -> JsonResponse | AuthModel:
    db_user = user_storage().get_user_by_phone(user.phone)
    if db_user is None or not user_storage().check_password(db_user, password=user.password):
        return JsonResponse({"detail": "Unauthorized"}, status=HTTPStatus.UNAUTHORIZED)

    return auth_storage().generate_access_and_refresh_tokens(db_user)


@api.post("/refresh")
def refresh(request: HttpRequest, token: RefreshModel) -> HttpResponseBadRequest | AuthModel:
    issued_token = auth_storage().get_token(token.refresh_token)
    if issued_token is None:
        return HttpResponseBadRequest("Invalid token")

    if issued_token.revoked:
        auth_storage().revoke_all_tokens(issued_token.user)
        return HttpResponseBadRequest("Invalid token")

    auth_storage().revoke_token(issued_token)
    if auth_storage().has_token_expired(token.refresh_token):
        return HttpResponseBadRequest("Invalid token")

    return auth_storage().generate_access_and_refresh_tokens(issued_token.user)
